# Freecodecamp

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `freecodecamp` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:freecodecamp, "~> 0.1.0"}
  ]
end
```

## Basic cli usage (local docs)

```bash
# install dependencies
mix deps.get

# generate docs, view them at doc/index.html
mix docs
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/freecodecamp](https://hexdocs.pm/freecodecamp).

